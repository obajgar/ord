# ORD

## Ondrej's Result Database (ORD)

Ord is a thin wrapper around a Mongo database into which one can submit results of machine learning experiments. Ord handles credential caching, is able to hold a temporary representation of the result, which one can gradually update during the whole duration of the experiment. In the end, this representation is submitted to the database. See methods on the OrdConnection class in ord.py to explore full functionality.

If you need to set up a ORD account on Ondrej's server, please contact Ondrej Bajgar (ondrej@bajgar.org).

Installation:
```bash
pip install git+https://gitlab.com/obajgar/ord.git
```