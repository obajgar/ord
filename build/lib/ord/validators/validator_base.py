class ValidatorBase(object):
    """
    Validator of result inputs. Should implement a validate method that takes results to be saved to ORD and raises
    an exception if they are not in correct format.
    """
    def validate(self, result_json):
        raise NotImplementedError("Abstract class.")
