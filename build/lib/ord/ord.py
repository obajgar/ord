import atexit
import collections
import getpass
import json
import os
import pickle
import re
import socket
import stat
import sys
import time

from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError

MONGO_SERVER = 'bajgar.org'
MONGO_PORT = 41600
DATABASE_NAME = 'ord'

DEFAULT_COLLECTION = "default_results"


def nested_update(d, u):
    """
    Similar to d.update(u) but also updates nested dicts instead of rewriting them.
    :param d: original dictionary to be modified
    :param u: update dict
    :return:
    """
    for k, v in u.items():
        if isinstance(v, collections.Mapping):
            d[k] = nested_update(d.get(k, {}), v)
        else:
            d[k] = v
    return d


class OrdConnection(object):

    def __init__(self, user=None, password=None, validators=tuple(), local_save_dir=None,
                 credentials_path='~/.ord_credentials', failed_uploads_cache='~/.ord_failed_uploads',
                 collection=DEFAULT_COLLECTION, offline=True, emergency_flush=True):
        """

        :param user: (optional) db username
        :param password: (optional) db password
        :param credentials_path: path from which username and password are fetched if they're not explicitly given

        """

        # This allows automatic submission upon destruction. Should be switched on once there are some valuable data
        # to submit. (Don't want to flood Ord with empty result templates from crashed trainings.)
        self._allow_emergency_flush = emergency_flush

        if user is not None and password is not None:
            u = user
            p = password
        elif user is not None or password is not None:
            raise ValueError("You can't provide just one of username/password.")
        else:
            u, p = self._get_credentials(credentials_path)

        if not offline:
            self._client = MongoClient(MONGO_SERVER, MONGO_PORT, username=u, password=p, authSource=DATABASE_NAME)
            self._collection = self._client.ord[collection]
        self.user = u
        self._validators = validators
        self._failed_uploads_cache = os.path.expanduser(failed_uploads_cache)
        self._local_save_dir = local_save_dir

        self._tmp_result = self._get_default_fields()
        self.update({'metadata': {'ord_collection': collection}})

        self._init_time = time.time()
        self._last_result_time = time.time()

        # File handle for printing to a local log
        self._local_log_fh = None
        self._local_save_successful = False

        self._offline = offline

        atexit.register(self.flush)


    def update(self, new_fields):
        """
        Update the temporary result using a nested dict update.
        WARNING: This does not submit the results to the database!
        :param new_fields: Fields to be added/updated
        :return:
        """
        nested_update(self._tmp_result, new_fields)

    def append_result(self, r):
        result = dict(r)
        t = time.time()
        result['duration_total'] = t - self._init_time
        result['duration_delta'] = t - self._last_result_time
        self._last_result_time = t
        self._tmp_result['results'].append(result)

    def _update_submission_time(self, result=None):
        if result is None:
            result = self._tmp_result

        t = time.time()
        nested_update(result, {'metadata': {
            'submission_time': time.strftime('%y/%m/%d %H:%M:%S', time.gmtime()),
            'duration': t - self._init_time
        }})

    def submit_results(self, result=None, reattempt=False):
        """
        Insert results into ORD
        :param result: JSON-like nested dict object
        :return:
        """

        if self._local_save_dir:
            self.save_locally()

        if self._offline:
            return self._local_save_successful

        if result is None:
            result = self._tmp_result

        if not reattempt:
            self._update_submission_time()
            self._validate(result)
        coll = self._client.ord[result['metadata']['ord_collection']]
        try:
            coll.insert_one(result)
            # Upload successful.
            self._allow_emergency_flush = False
            print("Results successfully submitted to ORD.")
            # Try to resubmit failed uploads.
            if not reattempt:
                self._upload_failed_results()

            return True
        except DuplicateKeyError:
            self._handle_duplicate_key(result, coll)
            return True
        except Exception as e:
            if not reattempt:
                print("ORD Error: Failed to save to the Mongo db. Temporarily saving to " + self._failed_uploads_cache)
                self._cache_save(result)
            print(e)
            return False

    def append_task(self, task_metadata):
        self._tmp_result['tasks'].append(task_metadata)

    def _get_credentials(self, path='~/.ord_credentials'):
        """
        Handles credential caching.
        :param path: cache path. A file with username on first line, password on second.
        :return:
        """
        cred_path = os.path.expanduser(path)

        try:
            with open(cred_path) as f:
                u, p = f.readlines()
                u = u.rstrip()
                p = p.rstrip()

        except IOError:
            print("ORD credentials not found in %s. Please provide them:" % cred_path)
            u = input("User: ")
            p = getpass.getpass()

            with open(cred_path,'w') as f:
                f.write(u+"\n"+p)

            os.chmod(cred_path, stat.S_IRUSR | stat.S_IWUSR)
            print("Credentials saved to "+cred_path)

        return u, p

    def _validate(self, results):
        for validator in self._validators:
            validator.validate(results)

    def _get_default_fields(self):
        # These may be overwritten by the results from the user:
        default_fields = {
            '_id': 'result_{}'.format(time.strftime('%y%m%d%H%M%S', time.gmtime())),
            'metadata': {
                'user': self.user,
                'host': socket.gethostname(),
                'argv': sys.argv,
                'init_time': time.strftime('%y/%m/%d %H:%M:%S', time.gmtime())
                },
            'results': [],
            'tasks': []
        }

        try:
            experiment_file = sys.modules['__main__'].__file__
            default_fields['experiment_file'] = experiment_file
        except AttributeError:
            pass

        return default_fields

    def _cache_save(self, results):
        if not os.path.exists(self._failed_uploads_cache):
            os.makedirs(self._failed_uploads_cache)

        cache_file = os.path.join(self._failed_uploads_cache, results['_id'] + ".pkl")

        with open(cache_file, 'wb') as fh:
            pickle.dump(results, fh)

    def _open_local_log(self):

        if not os.path.exists(self._local_save_dir):
            os.makedirs(self._local_save_dir)

        log_file_path = os.path.join(self._local_save_dir, self._tmp_result['_id'])

        i = 0
        suffix = '.json'

        while os.path.exists(log_file_path+suffix):
            i += 1
            suffix = "_{:03d}.json".format(i)

        self._local_log_fh = open(log_file_path + suffix, 'w')

    def save_locally(self):

        if self._local_log_fh is not None:
            self._local_log_fh.close()

        self._update_submission_time()

        self._open_local_log()
        json.dump(self._tmp_result, self._local_log_fh, default=lambda obj: 'NotSerializable')
        self._local_log_fh.close()

        self._local_save_successful = True

    # def append_to_local_log(self, new_log_data):
    #
    #     if self._local_log_fh is None:
    #         self._open_local_log()
    #         self._local_log_fh.write('[')
    #
    #     json.dump(new_log_data)
    #     self._local_log_fh.write(",\n")

    def _upload_failed_results(self):
        if not os.path.exists(self._failed_uploads_cache):
            return
        cached_files = os.listdir(self._failed_uploads_cache)
        for file in cached_files:
            path = os.path.join(self._failed_uploads_cache, file)
            print("Attempting to resubmit failed upload: "+path)
            with open(path, 'rb') as fh:
                result = pickle.load(fh)
            # The cached results should already be validated
            success = self.submit_results(result, reattempt=True)
            if success:
                os.remove(path)

    def enable_failure_flush(self):
        """
        Allows submission of _tmp_result to ord if training is interrupted before proper submission.
        This is switched off upon successful submission.
        :return:
        """
        self._allow_emergency_flush = True


    def _handle_duplicate_key(self, insert, collection):
        """
        Attach an integer suffix to the insert _id. Check that it indeed avoids conflict.
        :param insert:
        :return:
        """
        original_id = insert['_id']

        def get_used_suffixes():
            used_ids = self._collection.find({'_id': re.compile('^' + original_id)})
            used_suffixes = []
            for used_id in used_ids:
                used_suffixes.append(re.sub(original_id, '', used_id['_id']))

            return used_suffixes

        used_suffixes = get_used_suffixes()
        i = 1
        while True:
            new_suffix = '_' + str(i)
            if new_suffix not in used_suffixes:
                try:
                    new_id = original_id + new_suffix
                    insert['_id'] = new_id
                    collection.insert_one(insert)
                    break
                except DuplicateKeyError:
                    used_suffixes = get_used_suffixes()
                    pass
            i += 1

    def find(self, *args, collection=None, **kwargs):
        if collection is None:
            collection = self._collection
        else:
            collection = self._client.ord[collection]

        return collection.find(*args, **kwargs)

    def flush(self):
        if self._local_save_dir and not self._local_save_successful:
            self.save_locally()

        if self._allow_emergency_flush:
            self.submit_results()
