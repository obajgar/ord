from ..ord import OrdConnection
from collections import defaultdict
import pandas as pd
import glob
import json

# An example of how field mapping can look like. Leaves of the tree are labels in the resulting DataFrame.
default_rl_field_mapping = {"configuration": {'model/embedding_dim': 'ed',
                                              'model/gru_dim': 'gd',
                                              'model/activation_function': 'activation',
                                              'model/use_bias': 'use_bias',
                                              'model/dropout_keep_probability': 'dropout',
                                              'env/ws_id': 'ws',
                                              'env/arm_limit': 'arm_limit_train',
                                              'env_val/arm_limit': 'arm_limit_val',
                                              'trainer/update_grouping': 'update_grouping',
                                              'trainer/batch_size': 'batch',
                                              'trainer/train_update_count': 'train_update_count',
                                              'trainer/learning_rate': 'lr',
                                              'trainer/optimizer': 'optimizer'},
                            "metadata": {"duration": "duration"},
                            "results": {0: {"steps":"train_steps",
                                              "average_reward": "train_avg_rwd"},
                                        1: {"steps": "val_steps",
                                            "average_reward": "val_average_reward"}}
                            }


def extract_fields(db_results, field_mapping=default_rl_field_mapping):
    """

    :param db_results: an interator over nested dicts from which data are to be extracted
    :param buffer: a defaultdict into which the data will be saved (in place)
    :param field_mapping: nested dict mapping nested representation into the flat column structure of a DataFrame.
                          Leaves of the dict tree are column labels  (see example above)
    :return: None (results saved into the buffer)
    """
    result_buffer = defaultdict(lambda: list())

    def set_leaves_to_none(fields):
        for name, value in fields.items():
            if type(value) == str:
                result_buffer[value].append(None)
            elif type(value) == dict:
                set_leaves_to_none(value)
            else:
                raise ValueError("The values of the fields dict must be str or a dict.")

    def extract_fields_from_branch(data_branch, fields):
        # Extracts workspaces
        for name, value in fields.items():
            # Check whether we are in a leaf (target key)
            if type(value) == str:
                try:
                    result_buffer[value].append(data_branch[name])
                    # self.loc[index, value] = data_branch[name]
                except (KeyError, ValueError, TypeError, IndexError):
                    # TODO: Add support for required fields (skip example / abort if missing)
                    result_buffer[value].append(None)
                # except TypeError:
                #     print("TypeError branch")
                #     result[index, value] = data_branch

            elif type(value) in [dict, list]:
                # go one level deeper
                try:
                    extract_fields_from_branch(data_branch[name], fields[name])
                except (TypeError, KeyError, IndexError):
                    print("Failed extracting value {} for key {}".format(value, name))
                    set_leaves_to_none(fields[name])
                #     pass
            else:
                raise ValueError("The values of the fields dict must be str or a dict.")

    if isinstance(db_results, dict):
        db_results = [db_results]
    for entry in db_results:
        extract_fields_from_branch(entry, field_mapping)

    return result_buffer

def ord2dataframe(query, field_mapping=default_rl_field_mapping, collection='rl_results'):
    """

    :param query: a query passed to Mongo find
    :param field_mapping: nested dict tree representing which fields should be extracted and into which columns they
                          should be mapped.
    :param collection: ORD Mongo collection from which the results should be read.
    :return: pd.DataFrame with the extracted results
    """
    wc = OrdConnection(collection=collection)
    cursor = wc.find(query)

    extracted_data = extract_fields(cursor, field_mapping)

    return pd.DataFrame(extracted_data)

def json_logs_to_dataframe(path, field_mapping=default_rl_field_mapping):
    """

    :param path: glob regex to json logs
    :return:
    """
    files = glob.glob(path)

    results = [json.load(open(f)) for f in files]

    extracted_data = extract_fields(results, field_mapping)

    return pd.DataFrame(extracted_data)
