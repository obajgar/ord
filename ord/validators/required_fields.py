from ord.validators.validator_base import ValidatorBase


class RequiredFieldsValidator(ValidatorBase):
    def __init__(self, required_fields):
        """
        :param required_fields: list or dict specifying required fields in results json inserted via this connection
                        format: a nested-dict tree, whose keys should be contained in the results json. Leaves
                                can be (possibly empty) lists of required children of the given key.
                                Example: {"experiment_file":[],
                                          "metrics": ["total_reward", "average_reward"],
                                          "metaparams": {"trainer":
                                                            ["learning_rate",
                                                             "batch_size",
                                                             "optimizer"]
                                                         "model": []
                                                         }
                                         }
                                Note: The leaves of required_fields don't need to be leaves in the results tree
        :param required_fields:
        """
        self._required_fields = required_fields

    def validate(self, result_json):
        """
        Checks that results have fields specified in self.required_fields (see __init__ for format)
        :param results: dict with results
        :return: nothing or raises an exception
        """

        def check_fields(fields, d):
            if isinstance(fields, list):
                for f in fields:
                    d[f]
            elif isinstance(fields, dict):
                for f, v in fields.items():
                    check_fields(v, d[f])

        check_fields(self._required_fields, result_json)
