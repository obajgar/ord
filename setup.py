import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ord",
    version="0.2.0",
    author="Ondrej Bajgar",
    author_email="ondrej@bajgar.org",
    description="Ondrej's Result Database",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/obajgar/ord",
    packages=setuptools.find_packages(),
    install_requires=['pymongo', 'pandas'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent"
    ],
)
